MODULE SteamProp
!
    IMPLICIT NONE

    REAL(kind=8), PARAMETER :: R=0.461526  !kJ/kg-K
    REAL(kind=8), PARAMETER :: Tc=647.096  !K
    REAL(kind=8), PARAMETER :: pc=22.064   !MPa
    REAL(kind=8), PARAMETER :: rhoc=322.0  !kg/m^3

    REAL(kind=8),DIMENSION(5) :: nR2tR3

    DATA nR2tR3 / 0.34805185628969E3 ,  &
                 -0.11671859879975E1 ,  &
                  0.10192970039326E-2 , &
                  0.57254459862746E3 ,  &
                  0.13918839778870E2 /

    REAL(kind=8), PARAMETER :: psR1=16.53   !MPa
    REAL(kind=8), PARAMETER :: TsR1=1386.0  !K

    INTEGER,DIMENSION(34)         :: IR1
    INTEGER,DIMENSION(34)         :: JR1
    REAL(kind=8),DIMENSION(34)    :: nR1

    DATA IR1 /  0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 1 , &
                1 , 1 , 1 , 1 , 2 , 2 , 2 , 2 , 2 , 3 , &
                3 , 3 , 4 , 4 , 4 , 5 , 8 , 8 ,21 ,23 , &
               29 ,30 ,31 ,32  /

    DATA JR1 / -2 , -1 ,  0 ,  1 ,  2 ,  3 ,  4 ,  5 , -9 , -7 , &
               -1 ,  0 ,  1 ,  3 , -3 ,  0 ,  1 ,  3 , 17 , -4 , &
                0 ,  6 , -5 , -2 , 10 , -8 ,-11 , -6 ,-29 ,-31 , &
              -38 ,-39 ,-40 ,-41  /

    DATA nR1  / 0.14632971213167 ,    &
               -0.84548187169114 ,    &
               -0.37563603672040E1 ,  &
                0.33855169168385E1 ,  &
               -0.95791963387872 ,    &
                0.15772038513228 ,    &
               -0.16616417199501E-1 , &
                0.81214629983568E-3 , &
                0.28319080123804E-3 , &
               -0.60706301565874E-3 , &
               -0.18990068218419E-1 , &
               -0.32529748770505E-1 , &
               -0.21841717175414E-1 , &
               -0.52838357969930E-4 , &
               -0.47184321073267E-3 , &
               -0.30001780793026E-3 , &
                0.47661393906987E-4 , &
               -0.44141845330846E-5 , &
               -0.72694996297594E-15 ,&
               -0.31679644845054E-4 , &
               -0.28270797985312E-5 , &
               -0.85205128120103E-9 , &
               -0.22425281908000E-5 , &
               -0.65171222895601E-6 , &
               -0.14341729937924E-12 ,&
               -0.40516996860117E-6 , &
               -0.12734301741641E-8 , &
               -0.17424871230634E-9 , &
               -0.68762131295531E-18 ,&
                0.14478307828521E-19 ,&
                0.26335781662795E-22 ,&
               -0.11947622640071E-22 ,&
                0.18228094581404E-23 ,&
               -0.93537087292458E-25  /

    REAL(kind=8), PARAMETER :: hsR1=2500.0  !kJ/kg

    INTEGER,DIMENSION(20) :: TIR1
    INTEGER,DIMENSION(20) :: TJR1
    REAL(kind=8),DIMENSION(20)    :: TnR1

    DATA TIR1 / 0 , 0 , 0 , 0 , 0 , 0 , 1 , 1 , 1 , 1 , &
                1 , 1 , 1 , 2 , 2 , 3 , 3 , 4 , 5 , 6  /

    DATA TJR1 / 0 ,  1 ,  2 ,  6 , 22 , 32 ,  0 ,  1 ,  2 ,  3 , &
                4 , 10 , 32 , 10 , 32 , 10 , 32 , 32 , 32 , 32  /

    DATA TnR1 / -0.23872489924521E3 ,  &
                 0.40421188637945E3 ,  &
                 0.11349746881718E3 ,  &
                -0.58457616048039E1 ,  &
                -0.15285482413140E-3 , &
                -0.10866707695377E-5 , &
                -0.13391744872602E2 ,  &
                 0.43211039183559E2 ,  &
                -0.54010067170506E2 ,  &
                 0.30535892203916E2 ,  &
                -0.65964749423638E1 ,  &
                 0.93965400878363E-2 , &
                 0.11573647505340E-6 , &
                -0.25858641282073E-4 , &
                -0.40644363084799E-8 , &
                 0.66456186191635E-7 , &
                 0.80670734103027E-10 ,&
                -0.93477771213947E-12 ,&
                 0.58265442020601E-14 ,&
                -0.15020185953503E-16  /

    REAL(kind=8), PARAMETER :: psR2=1.0    !MPa
    REAL(kind=8), PARAMETER :: TsR2=540.0  !K

    INTEGER,DIMENSION(9) :: J0R2
    REAL(kind=8),DIMENSION(9)    :: n0R2

    DATA J0R2 / 0 , 1 ,-5 ,-4 ,-3 ,-2 ,-1 , 2 , 3  /
    DATA n0R2 / -0.96927686500217E1 ,  &
                 0.10086655968018E2 ,  &
                -0.56087911283020E-2 , &
                 0.71452738081455E-1 , &
                -0.40710498223928 ,    &
                 0.14240819171444E1 ,  &
                -0.43839511319450E1 ,  &
                -0.28408632460772 ,    &
                 0.21268463753307E-1  /

    INTEGER,DIMENSION(43) :: IrR2
    INTEGER,DIMENSION(43) :: JrR2
    REAL(kind=8),DIMENSION(43)    :: nrR2

    DATA IrR2 / 1 , 1 , 1 , 1 , 1 , 2 , 2 , 2 , 2 , 2 , &
                3 , 3 , 3 , 3 , 3 , 4 , 4 , 4 , 5 , 6 , &
                6 , 6 , 7 , 7 , 7 , 8 , 8 , 9 ,10 ,10 , &
               10 ,16 ,16 ,18 ,20 ,20 ,20 ,21 ,22 ,23 , &
               24 ,24 ,24  /

    DATA JrR2 / 0 , 1 , 2 , 3 , 6 , 1 , 2 , 4 , 7 ,36 , &
                0 , 1 , 3 , 6 ,35 , 1 , 2 , 3 , 7 , 3 , &
               16 ,35 , 0 ,11 ,25 , 8 ,36 ,13 , 4 ,10 , &
               14 ,29 ,50 ,57 ,20 ,35 ,48 ,21 ,53 ,39 , &
               26 ,40 ,58  /

    DATA nrR2 / -0.17731742473213E-2 ,  &
                -0.17834862292358E-1 ,  &
                -0.45996013696365E-1 ,  &
                -0.57581259083432E-1 ,  &
                -0.50325278727930E-1 ,  &
                -0.33032641670203E-4 ,  &
                -0.18948987516315E-3 ,  &
                -0.39392777243355E-2 ,  &
                -0.43797295650573E-1 ,  &
                -0.26674547914087E-4 ,  &
                 0.20481737692309E-7 ,  &
                 0.43870667284435E-6 ,  &
                -0.32277677238570E-4 ,  &
                -0.15033924542148E-2 ,  &
                -0.40668253562649E-1 ,  &
                -0.78847309559367E-9 ,  &
                 0.12790717852285E-7 ,  &
                 0.48225372718507E-6 ,  &
                 0.22922076337661E-5 ,  &
                -0.16714766451061E-10 , &
                -0.21171472321355E-2 ,  &
                -0.23895741934104E2 ,   &
                -0.59059564324270E-17 , &
                -0.12621808899101E-5 ,  &
                -0.38946842435739E-1 ,  &
                 0.11256211360459E-10 , &
                -0.82311340897998E1 ,   &
                 0.19809712802088E-7 ,  &
                 0.10406965210174E-18 , &
                -0.10234747095929E-12 , &
                -0.10018179379511E-8 ,  &
                -0.80882908646985E-10 , &
                 0.10693031879409 ,     &
                -0.33662250574171 ,     &
                 0.89185845355421E-24 , &
                 0.30629316876232E-12 , &
                -0.42002467698208E-5 ,  &
                -0.59056029685639E-25 , &
                 0.37826947613457E-5 ,  &
                -0.12768608934681E-14 , &
                 0.73087610595061E-28 , &
                 0.55414715350778E-16 , &
                -0.94369707241210E-6  /

    REAL(kind=8),DIMENSION(5) :: nR2btR2c

    DATA nR2btR2c / 0.90584278514723E3 ,  &
                   -0.67955786399241 ,    &
                    0.12809002730136E-3 , &
                    0.26526571908428E4 ,  &
                    0.45257578905948E1  /

    INTEGER,DIMENSION(34) :: TIR2a
    INTEGER,DIMENSION(34) :: TJR2a
    REAL(kind=8),DIMENSION(34)    :: TnR2a

    DATA TIR2a / 0 , 0 , 0 , 0 , 0 , 0 , 1 , 1 , 1 , 1 , &
                 1 , 1 , 1 , 1 , 1 , 2 , 2 , 2 , 2 , 2 , &
                 2 , 2 , 2 , 3 , 3 , 4 , 4 , 4 , 5 , 5 , &
                 5 , 6 , 6 , 7  /

    DATA TJR2a / 0 , 1 , 2 , 3 , 7 ,20 , 0 , 1 , 2 , 3 , &
                 7 , 9 ,11 ,18 ,44 , 0 , 2 , 7 ,36 ,38 , &
                40 ,42 ,44 ,24 ,44 ,12 ,32 ,44 ,32 ,36 , &
                42 ,34 ,44 ,28  /

    DATA TnR2a / 0.10898952318288E4 , &
                 0.84951654495535E3 , &
                -0.10781748091826E3 , &
                 0.33153654801263E2 , &
                -0.74232016790248E1 , &
                 0.11765048724356E2 , &
                 0.18445749355790E1 , &
                -0.41792700549624E1 , &
                 0.62478196935812E1 , &
                -0.17344563108114E2 , &
                -0.20058176862096E3 , &
                 0.27196065473796E3 , &
                -0.45511318285818E3 , &
                 0.30919688604755E4 , &
                 0.25226640357872E6 , &
                -0.61707422868339E-2 ,&
                -0.31078046629583 ,   &
                 0.11670873077107E2 , &
                 0.12812798404046E9 , &
                -0.98554909623276E9 , &
                 0.28224546973002E10 , &
                -0.35948971410703E10 , &
                 0.17227349913197E10 , &
                -0.13551334240775E5 , &
                 0.12848734664650E8 , &
                 0.13865724283226E1 , &
                 0.23598832556514E6 , &
                -0.13105236545054E8 , &
                 0.73999835474766E4 , &
                -0.55196697030060E6 , &
                 0.37154085996233E7 , &
                 0.19127729239660E5 , &
                -0.41535164835634E6 , &
                -0.62459855192507E2  /

    INTEGER,DIMENSION(38) :: TIR2b
    INTEGER,DIMENSION(38) :: TJR2b
    REAL(kind=8),DIMENSION(38)    :: TnR2b

    DATA TIR2b / 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 1 , &
                 1 , 1 , 1 , 1 , 1 , 1 , 2 , 2 , 2 , 2 , &
                 3 , 3 , 3 , 3 , 4 , 4 , 4 , 4 , 4 , 4 , &
                 5 , 5 , 5 , 6 , 7 , 7 , 9 , 9  /


    DATA TJR2b / 0 , 1 , 2 ,12 ,18 ,24 ,28 ,40 , 0 , 2 , &
                 6 ,12 ,18 ,24 ,28 ,40 , 2 , 8 ,18 ,40 , &
                 1 , 2 ,12 ,24 , 2 ,12 ,18 ,24 ,28 ,40 , &
                18 ,24 ,40 ,28 , 2 ,28 , 1 ,40  /

    DATA TnR2b / 0.14895041079516E4 ,  &
                 0.74307798314034E3 ,  &
                -0.97708318797837E2 ,  &
                 0.24742464705674E1 ,  &
                -0.63281320016026 ,    &
                 0.11385952129658E1 ,  &
                -0.47811863648625 ,    &
                 0.85208123431544E-2 , &
                 0.93747147377932 ,    &
                 0.33593118604916E1 ,  &
                 0.33809355601454E1 ,  &
                 0.16844539671904 ,    &
                 0.73875745236695 ,    &
                -0.47128737436186 ,    &
                 0.15020273139707 ,    &
                -0.21764114219750E-2 , &
                -0.21810755324761E-1 , &
                -0.10829784403677 ,    &
                -0.46333324635812E-1 , &
                 0.71280351959551E-4 , &
                 0.11032831789999E-3 , &
                 0.18955248387902E-3 , &
                 0.30891541160537E-2 , &
                 0.13555504554949E-2 , &
                 0.28640237477456E-6 , &
                -0.10779857357512E-4 , &
                -0.76462712454814E-4 , &
                 0.14052392818316E-4 , &
                -0.31083814331434E-4 , &
                -0.10302738212103E-5 , &
                 0.28217281635040E-6 , &
                 0.12704902271945E-5 , &
                 0.73803353468292E-7 , &
                -0.11030139238909E-7 , &
                -0.81456365207833E-13 ,&
                -0.25180545682962E-10 ,&
                -0.17565233969407E-17 ,&
                 0.86934156344163E-14  /

    INTEGER,DIMENSION(23) :: TIR2c
    INTEGER,DIMENSION(23) :: TJR2c
    REAL(kind=8),DIMENSION(23)    :: TnR2c

    DATA TIR2c /-7 ,-7 ,-6 ,-6 ,-5 ,-5 ,-2 ,-2 ,-1 ,-1 , &
                 0 , 0 , 1 , 1 , 2 , 6 , 6 , 6 , 6 , 6 , &
                 6 , 6 , 6  /

    DATA TJR2c / 0 , 4 , 0 , 2 , 0 , 2 , 0 , 1 , 0 , 2 , &
                 0 , 1 , 4 , 8 , 4 , 0 , 1 , 4 ,10 ,12 , &
                16 ,20 ,22  /

    DATA TnR2c / -0.32368398555242E13 , &
                  0.73263350902181E13 , &
                  0.35825089945447E12 , &
                 -0.58340131851590E12 , &
                 -0.10783068217470E11 , &
                  0.20825544563171E11 , &
                  0.61074783564516E6 ,  &
                  0.85977722535580E6 ,  &
                 -0.25745723604170E5 ,  &
                  0.31081088422714E5 ,  &
                  0.12082315865936E4 ,  &
                  0.48219755109255E3 ,  &
                  0.37966001272486E1 ,  &
                 -0.10842984880077E2 ,  &
                 -0.45364172676660E-1 , &
                  0.14559115658698E-12 ,&
                  0.11261597407230E-11 ,&
                 -0.17804982240686E-10 ,&
                  0.12324579690832E-6 , &
                 -0.11606921130984E-5 , &
                  0.27846367088554E-4 , &
                 -0.59270038474176E-3 , &
                  0.12918582991878E-2  /

    INTEGER,DIMENSION(40) :: IR3
    INTEGER,DIMENSION(40) :: JR3
    REAL(kind=8),DIMENSION(40)    :: nR3

    DATA IR3 /  0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 1 , &
                1 , 1 , 2 , 2 , 2 , 2 , 2 , 2 , 3 , 3 , &
                3 , 3 , 3 , 4 , 4 , 4 , 4 , 5 , 5 , 5 , &
                6 , 6 , 6 , 7 , 8 , 9 , 9 ,10 ,10 ,11  /

    DATA JR3 /  0 , 0 , 1 , 2 , 7 ,10 ,12 ,23 , 2 , 6 , &
               15 ,17 , 0 , 2 , 6 , 7 ,22 ,26 , 0 , 2 , &
                4 ,16 ,26 , 0 , 2 , 4 ,26 , 1 , 3 ,26 , &
                0 , 2 ,26 , 2 ,26 , 2 ,26 , 0 , 1 ,26  /

    DATA nR3/    0.10658070028513E1 , &
                -0.15732845290239E2 , &
                 0.20944396974307E2 , &
                -0.76867707878716E1 , &
                 0.26185947787954E1 , &
                -0.28080781148620E1 , &
                 0.12053369696517E1 , &
                -0.84566812812502E-2 ,&
                -0.12654315477714E1 , &
                -0.11524407806681E1 , &
                 0.88521043984318 ,   &
                -0.64207765181607 ,   &
                 0.38493460186671 ,   &
                -0.85214708824206 ,   &
                 0.48972281541877E1 , &
                -0.30502617256965E1 , &
                 0.39420536879154E-1 ,&
                 0.12558408424308 ,   &
                -0.27999329698710 ,   &
                 0.13899799569460E1 , &
                -0.20189915023570E1 , &
                -0.82147637173963E-2 ,&
                -0.47596035734923 ,   &
                 0.43984074473500E-1 ,&
                -0.44476435428739 ,   &
                 0.90572070719733 ,   &
                 0.70522450087967 ,   &
                 0.10770512626332 ,   &
                -0.32913623258954 ,   &
                -0.50871062041158 ,   &
                -0.22175400873096E-1 ,&
                 0.94260751665092E-1 ,&
                 0.16436278447961 ,   &
                -0.13503372241348E-1 ,&
                -0.14834345352472E-1 ,&
                 0.57922953628084E-3 ,&
                 0.32308904703711E-2 ,&
                 0.80964802996215E-4 ,&
                -0.16557679795037E-3 ,&
                -0.44923899061815E-4  /

    REAL(kind=8),DIMENSION(10)    :: nR4

    DATA nR4/    0.11670521452767E4 , &
                -0.72421316703206E6 , &
                -0.17073846940092E2 , &
                 0.12020824702470E5 , &
                -0.32325550322333E7 , &
                 0.14915108613530E2 , &
                -0.48232657361591E4 , &
                 0.40511340542057E6 , &
                -0.23855557567849 ,   &
                 0.65017534844798E3   /

    REAL(kind=8),DIMENSION(4)      :: mu0H
    REAL(kind=8),DIMENSION(6,7)    :: mu1H

    DATA mu0H/  1.67752 ,  &
                2.20462 ,  &
                0.6366564 ,&
               -0.241605   /

    DATA mu1H /  5.20094E-1 , 8.50895E-2 ,-1.08374    ,-2.89555E-1 , 0.0        ,  0.0       , &
                 2.22531E-1 , 9.99115E-1 , 1.88797    , 1.26613    , 0.0        , 1.20573E-1 , &
                -2.81378E-1 ,-9.06851E-1 ,-7.72479E-1 ,-4.89837E-1 ,-2.57040E-1 , 0.0        , &
                 1.61913E-1 , 2.57399E-1 , 0.0        , 0.0        , 0.0        , 0.0        , &
                -3.25372E-2 , 0.0        , 0.0        , 6.98452E-2 , 0.0        , 0.0        , &
                 0.0        , 0.0        , 0.0        , 0.0        , 8.72102E-3 , 0.0        , &
                 0.0        , 0.0        , 0.0        ,-4.35673E-3 , 0.0        ,-5.93264E-4  /

   CONTAINS

  !P(T) Boundary between regions 2 and 3
    FUNCTION PR23(T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8) :: PR23

        PR23=nR2tR3(1)+nR2tR3(2)*T+nR2tR3(3)*T*T
        RETURN
    END FUNCTION PR23

   !T(p) Boundary between regions 2 and 3
    FUNCTION TR23(p)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
       !Local Variables
        REAL(kind=8) :: TR23

        TR23=nR2tR3(4)+SQRT((p-nR2tR3(5))/nR2tR3(3))
        RETURN
    END FUNCTION TR23

   !G(pi,tau) in Region 1
    FUNCTION GR1(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: GR1
        INTEGER :: i

        GR1=0.0 
        DO i=1,34
            GR1=GR1+nR1(i)*((7.1 -pi)**IR1(i))*((tau-1.222 )**JR1(i))
        END DO
        RETURN
    END FUNCTION GR1

   !dGdp(pi,tau) in Region 1
    FUNCTION dGdpR1(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: dGdpR1
        INTEGER :: i

        dGdpR1=0.0 
        DO i=1,34
            dGdpR1=dGdpR1-nR1(i)*IR1(i)*((7.1 -pi)**(IR1(i)-1))*((tau-1.222 )**JR1(i))
        END DO
        RETURN
    END FUNCTION dGdpR1

   !d2Gdp2(pi,tau) in Region 1
    FUNCTION d2Gdp2R1(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: d2Gdp2R1
        INTEGER :: i

        d2Gdp2R1=0.0 
        DO i=1,34
            d2Gdp2R1=d2Gdp2R1+nR1(i)*IR1(i)*(IR1(i)-1)*((7.1 -pi)**(IR1(i)-2))*((tau-1.222 )**JR1(i))
        END DO
        RETURN
    END FUNCTION d2Gdp2R1

   !dGdt(pi,tau) in Region 1
    FUNCTION dGdtR1(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: dGdtR1
        INTEGER :: i

        dGdtR1=0.0 
        DO i=1,34
            dGdtR1=dGdtR1+nR1(i)*((7.1 -pi)**IR1(i))*JR1(i)*((tau-1.222 )**(JR1(i)-1 ))
        END DO
        RETURN
    END FUNCTION dGdtR1

   !d2Gdt2(pi,tau) in Region 1
    FUNCTION d2Gdt2R1(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: d2Gdt2R1
        INTEGER :: i

        d2Gdt2R1=0.0 
        DO i=1,34
            d2Gdt2R1=d2Gdt2R1+nR1(i)*((7.1 -pi)**IR1(i))*JR1(i)*(JR1(i)-1 )*((tau-1.222 )**(JR1(i)-2 ))
        END DO
        RETURN
    END FUNCTION d2Gdt2R1

   !theta(pi,eta) in Region 1
    FUNCTION thetaR1(pi,eta)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: eta
       !Local Variables
        REAL(kind=8)    :: thetaR1
        INTEGER :: i

        thetaR1=0.0 
        DO i=1,20
            thetaR1=thetaR1+TnR1(i)*(pi**TIR1(i))*((eta+1.0 )**TJR1(i))
        END DO
        RETURN
    END FUNCTION thetaR1

   !v(p,T) in Region 1
    FUNCTION vR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p ! in MPa
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: vR1
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dGdp

        pi=p/psR1
        tau=TsR1/T
        dGdp=dGdpR1(pi,tau)
        vR1=0.001 *R*T*dGdp/psR1
        RETURN
    END FUNCTION vR1

   !rho(p,T) in Region 1
    FUNCTION rhoR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p ! in MPa
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: rhoR1
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dGdp

        pi=p/psR1
        tau=TsR1/T
        dGdp=dGdpR1(pi,tau)
        rhoR1=1000.0 *psR1/(R*T*dGdp)
        RETURN
    END FUNCTION rhoR1

   !h(p,T) in Region 1
    FUNCTION hR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: hR1
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dGdt

        pi=p/psR1
        tau=TsR1/T
        dGdt=dGdtR1(pi,tau)
        hR1=R*T*tau*dGdt
        RETURN
    END FUNCTION hR1

   !u(p,T) in Region 1
    FUNCTION uR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: uR1
        REAL(kind=8)    :: pMPa
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dGdp
        REAL(kind=8)    :: dGdt

        pMPa=p*1.0E-6 
        pi=pMPa/psR1
        tau=TsR1/T
        dGdp=dGdpR1(pi,tau)
        dGdt=dGdtR1(pi,tau)
        uR1=R*T*(tau*dGdt-pi*dGdp)
        RETURN
    END FUNCTION uR1

   !s(p,T) in Region 1
    FUNCTION sR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: sR1
        REAL(kind=8)    :: pMPa
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: G
        REAL(kind=8)    :: dGdt

        pMPa=p*1.0E-6 
        pi=pMPa/psR1
        tau=TsR1/T
        G=GR1(pi,tau)
        dGdt=dGdtR1(pi,tau)
        sR1=R*(tau*dGdt-G)
        RETURN
    END FUNCTION sR1

   !cp(p,T) in Region 1
    FUNCTION cpR1(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: cpR1
        REAL(kind=8)    :: pMPa
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: d2Gdt2

        pMPa=p*1.0E-6 
        pi=pMPa/psR1
        tau=TsR1/T
        d2Gdt2=d2Gdt2R1(pi,tau)
        cpR1=-R*tau*tau*d2Gdt2
        RETURN
    END FUNCTION cpR1

   !T(p,h) in Region 1
    FUNCTION TR1(p,h)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
        REAL(kind=8),INTENT(in)  :: h
       !Local Variables
        REAL(kind=8)    :: TR1
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: eta

        pi=p*1.0E-6 
        eta=h/hsR1
        TR1=thetaR1(pi,eta)
        RETURN
    END FUNCTION TR1

   !G(pi,tau) in Region 2
    FUNCTION GR2(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: GR2
        REAL(kind=8)    :: G0R2
        REAL(kind=8)    :: GrR2
        INTEGER :: i

        G0R2=log(pi)
        DO i=1,9
            G0R2=G0R2+n0R2(i)*(tau**J0R2(i))
        END DO
        GrR2=0.0 
        DO i=1,43
            GrR2=GrR2+nrR2(i)*(pi**IrR2(i))*((tau-0.5 )**JrR2(i))
        END DO
        GR2=G0R2+GrR2
        RETURN
    END FUNCTION GR2

   !dGdp(pi,tau) in Region 2
    FUNCTION dGdpR2(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: dGdpR2
        REAL(kind=8)    :: dG0dp
        REAL(kind=8)    :: dGrdp
        INTEGER :: i

        dG0dp=1.0 /pi
        dGrdp=0.0 
        DO i=1,43
            dGrdp=dGrdp+nrR2(i)*IrR2(i)*(pi**(IrR2(i)-1 ))*((tau-0.5 )**JrR2(i))
        END DO
        dGdpR2=dG0dp+dGrdp
        RETURN
    END FUNCTION dGdpR2

   !dGdt(pi,tau) in Region 2
    FUNCTION dGdtR2(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: dGdtR2
        REAL(kind=8)    :: dG0dt
        REAL(kind=8)    :: dGrdt
        INTEGER :: i

        dG0dt=0.0 
        DO i=1,9
            dG0dt=dG0dt+n0R2(i)*J0R2(i)*(tau**(J0R2(i)-1 ))
        END DO
        dGrdt=0.0 
        DO i=1,43
            dGrdt=dGrdt+nrR2(i)*(pi**IrR2(i))*JrR2(i)*((tau-0.5 )**(JrR2(i)-1 ))
        END DO
        dGdtR2=dG0dt+dGrdt
        RETURN
    END FUNCTION dGdtR2

   !dG2dt2(pi,tau) in Region 2
    FUNCTION d2Gdt2R2(pi,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: d2Gdt2R2
        REAL(kind=8)    :: d2G0dt2
        REAL(kind=8)    :: d2Grdt2
        INTEGER :: i

        d2G0dt2=0.0 
        DO i=1,9
            d2G0dt2=d2G0dt2+n0R2(i)*J0R2(i)*(J0R2(i)-1 )*(tau**(J0R2(i)-2 ))
        END DO
        d2Grdt2=0.0 
        DO i=1,43
            d2Grdt2=d2Grdt2+nrR2(i)*(pi**IrR2(i))*JrR2(i)*(JrR2(i)-1 )*((tau-0.5 )**(JrR2(i)-2 ))
        END DO
        d2Gdt2R2=d2G0dt2+d2Grdt2
        RETURN
    END FUNCTION d2Gdt2R2

   !theta(pi,tau) in Region 2a
    FUNCTION thetaR2a(pi,eta)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: eta
       !Local Variables
        REAL(kind=8)    :: thetaR2a
        INTEGER :: i

        thetaR2a=0.0 
        DO i=1,34
            thetaR2a=thetaR2a+TnR2a(i)*(pi**TIR2a(i))*((eta-2.1 )**TJR2a(i))
        END DO
        RETURN
    END FUNCTION thetaR2a

   !theta(pi,tau) in Region 2b
    FUNCTION thetaR2b(pi,eta)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: eta
       !Local Variables
        REAL(kind=8)    :: thetaR2b
        INTEGER :: i

        thetaR2b=0.0 
        DO i=1,38
            thetaR2b=thetaR2b+TnR2b(i)*((pi-2.0 )**TIR2b(i))*((eta-2.6 )**TJR2b(i))
        END DO
        RETURN
    END FUNCTION thetaR2b

   !theta(pi,tau) in Region 2c
    FUNCTION thetaR2c(pi,eta)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: pi
        REAL(kind=8),INTENT(in)  :: eta
       !Local Variables
        REAL(kind=8)    :: thetaR2c
        INTEGER :: i

        thetaR2c=0.0 
        DO i=1,23
            thetaR2c=thetaR2c+TnR2c(i)*((pi+25.0 )**TIR2c(i))*((eta-1.8 )**TJR2c(i))
        END DO
        RETURN
    END FUNCTION thetaR2c

   !v(p,T) in Region 2
    FUNCTION vR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p ! in MPa
        REAL(kind=8),INTENT(in)  :: T ! in K
       !Local Variables
        REAL(kind=8)    :: vR2
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dGdp

        pi=p/psR2
        tau=TsR2/T
        dGdp=dGdpR2(pi,tau)
        vR2=0.001 *R*T*dGdp/psR2
        RETURN
    END FUNCTION vR2

   !rho(p,T) in Region 2
    FUNCTION rhoR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p ! in MPa
        REAL(kind=8),INTENT(in)  :: T ! in K
       !Local Variables
        REAL(kind=8)    :: rhoR2
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dGdp

        pi=p/psR2
        tau=TsR2/T
        dGdp=dGdpR2(pi,tau)
        rhoR2=1000.0 *psR2/(R*T*dGdp)
        RETURN
    END FUNCTION rhoR2

   !h(p,T) in Region 2
    FUNCTION hR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: hR2
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dGdt

        pi=p/psR2
        tau=TsR2/T
        dGdt=dGdtR2(pi,tau)
        hR2=R*T*tau*dGdt
        RETURN
    END FUNCTION hR2

   !u(p,T) in Region 2
    FUNCTION uR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: uR2
        REAL(kind=8)    :: pMPa
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dGdp
        REAL(kind=8)    :: dGdt

        pMPa=p*1.0E-6 
        pi=pMPa/psR2
        tau=TsR2/T
        dGdp=dGdpR2(pi,tau)
        dGdt=dGdtR2(pi,tau)
        uR2=R*T*(tau*dGdt-pi*dGdp)
        RETURN
    END FUNCTION uR2

   !s(p,T) in Region 2
    FUNCTION sR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: sR2
        REAL(kind=8)    :: pMPa
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: G
        REAL(kind=8)    :: dGdt

        pMPa=p*1.0E-6 
        pi=pMPa/psR2
        tau=TsR2/T
        G=GR2(pi,tau)
        dGdt=dGdtR2(pi,tau)
        sR2=R*(tau*dGdt-G)
        RETURN
    END FUNCTION sR2

   !cp(p,T) in Region 2
    FUNCTION cpR2(p,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: cpR2
        REAL(kind=8)    :: pMPa
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: d2Gdt2

        pMPa=p*1.0E-6 
        pi=pMPa/psR2
        tau=TsR2/T
        d2Gdt2=d2Gdt2R2(pi,tau)
        cpR2=-R*tau*tau*d2Gdt2
        RETURN
    END FUNCTION cpR2

   !pi(h) Boundary between Region 2b and 2c
    FUNCTION piR2bc(h)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: h
       !Local Variables
        REAL(kind=8)    :: piR2bc
        piR2bc=nR2btR2c(1)+nR2btR2c(2)*h+nR2btR2c(3)*h*h
        RETURN
    END FUNCTION piR2bc

   !T(p,h) in Region 2
    FUNCTION TR2(p,h)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
        REAL(kind=8),INTENT(in)  :: h
       !Local Variables
        REAL(kind=8)    :: TR2
        REAL(kind=8)    :: pi
        REAL(kind=8)    :: eta
        REAL(kind=8)    :: pib

        pi=p*1.0E-6 
        eta=h/2000.0 
        IF (pi>4.0 ) THEN
            pib=piR2bc(h)
            IF (pi<pib) THEN
                TR2=thetaR2b(pi,eta)
            ELSE
                TR2=thetaR2c(pi,eta)
            END IF
        ELSE
            TR2=thetaR2a(pi,eta)
        END IF
        RETURN
    END FUNCTION TR2

   !f(del,tau) in Region 3
    FUNCTION fR3(del,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: del
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: fR3
        INTEGER :: i

        fR3=nR3(1)*log(del)
        DO i=2,40
            fR3=fR3+nR3(i)*(del**IR3(i))*(tau**JR3(i))
        END DO
        RETURN
    END FUNCTION fR3

   !dfddel(del,tau) in Region 3
    FUNCTION dfddelR3(del,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: del
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: dfddelR3
        INTEGER :: i

        dfddelR3=nR3(1)/del
        DO i=2,40
            dfddelR3=dfddelR3+nR3(i)*IR3(i)*(del**(IR3(i)-1 ))*(tau**JR3(i))
        END DO
        RETURN
    END FUNCTION dfddelR3

   !dfdtau(del,tau) in Region 3
    FUNCTION dfdtauR3(del,tau)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: del
        REAL(kind=8),INTENT(in)  :: tau
       !Local Variables
        REAL(kind=8)    :: dfdtauR3
        INTEGER :: i

        dfdtauR3=0.0 
        DO i=2,40
            dfdtauR3=dfdtauR3+nR3(i)*(del**IR3(i))*JR3(i)*(tau**(JR3(i)-1 ))
        END DO
        RETURN
    END FUNCTION dfdtauR3

   !p(rho,T) in Region 3
    FUNCTION pR3(rho,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: rho
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: pR3
        REAL(kind=8)    :: pMPa
        REAL(kind=8)    :: del
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dfddel

        del=rho/rhoc
        tau=Tc/T
        dfddel=dfddelR3(del,tau)
        pMPa=0.001 *rho*R*T*del*dfddel
        pR3=pMPa*1.0E6 
        RETURN
    END FUNCTION pR3

   !h(rho,T) in Region 3
    FUNCTION hR3(rho,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: rho
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: hR3
        REAL(kind=8)    :: del
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dfdtau
        REAL(kind=8)    :: dfddel

        del=rho/rhoc
        tau=Tc/T
        dfdtau=dfdtauR3(del,tau)
        dfddel=dfddelR3(del,tau)
        hR3=R*T*(tau*dfdtau+del*dfddel)
        RETURN
    END FUNCTION hR3


   !u(rho,T) in Region 3
    FUNCTION uR3(rho,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: rho
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: uR3
        REAL(kind=8)    :: del
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: dfdtau

        del=rho/rhoc
        tau=Tc/T
        dfdtau=dfdtauR3(del,tau)
        uR3=R*Tc*dfdtau
        RETURN
    END FUNCTION uR3

   !s(rho,T) in Region 3
    FUNCTION sR3(rho,T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: rho
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: sR3
        REAL(kind=8)    :: del
        REAL(kind=8)    :: tau
        REAL(kind=8)    :: f
        REAL(kind=8)    :: dfdtau

        del=rho/rhoc
        tau=Tc/T
        dfdtau=dfdtauR3(del,tau)
        f=fR3(del,tau)
        sR3=R*(tau*dfdtau-f)
        RETURN
    END FUNCTION sR3

   !psat(T) Region 4
    FUNCTION psat(T)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: T
       !Local Variables
        REAL(kind=8)    :: psat
        REAL(kind=8)    :: pMPa
        REAL(kind=8)    :: nu
        REAL(kind=8)    :: A
        REAL(kind=8)    :: B
        REAL(kind=8)    :: C

        nu=T+nR4(9)/(T-nR4(10))
        A=       nu*nu+nR4(1)*nu+nR4(2)
        B=nR4(3)*nu*nu+nR4(4)*nu+nR4(5)
        C=nR4(6)*nu*nu+nR4(7)*nu+nR4(8)

        pMPa=(2.0 *C/(sqrt(B*B-4.0 *A*C)-B))**4 
        psat=pMPa*1.0E6 
        RETURN
    END FUNCTION psat

   !Tsat(p) Region 4
    FUNCTION Tsat(p)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: p
       !Local Variables
        REAL(kind=8)    :: Tsat
        REAL(kind=8)    :: beta
        REAL(kind=8)    :: D
        REAL(kind=8)    :: E
        REAL(kind=8)    :: F
        REAL(kind=8)    :: G

        beta=p**0.25 
        E=       beta*beta+nR4(3)*beta+nR4(6)
        F=nR4(1)*beta*beta+nR4(4)*beta+nR4(7)
        G=nR4(2)*beta*beta+nR4(5)*beta+nR4(8)
        D=-2.0 *G/(sqrt(F*F-4.0 *E*G)+F)

        Tsat=0.5 *(nR4(10)+D-sqrt((nR4(10)+D)*(nR4(10)+D)-4.0 *(nR4(9)+nR4(10)*D)))
        RETURN
    END FUNCTION Tsat

   !x12(h,p)
    FUNCTION x12(h,p)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: h
        REAL(kind=8),INTENT(in)  :: p
       !Local Variables
        REAL(kind=8)    :: x12
        REAL(kind=8)    :: Ts
        REAL(kind=8)    :: hfs
        REAL(kind=8)    :: hgs
        REAL(kind=8)    :: hfg

        Ts=Tsat(p)
        hfs=hR1(p,Ts)
        hgs=hR2(p,Ts)
        hfg=hgs-hfs
        x12=(h-hfs)/hfg

        RETURN
    END FUNCTION x12

   !mu0(T)
    FUNCTION mu0(Tb)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: Tb
       !Local Variables
        REAL(kind=8)    :: mu0
        REAL(kind=8)    :: den
        INTEGER :: i

        den=0.0 
        DO i=1,4
            den=den+mu0H(i)/(Tb**(i-1 ))
        END DO
        mu0=100.0 *sqrt(Tb)/den
        RETURN
    END FUNCTION mu0

   !mu1(T)
    FUNCTION mu1(Tb,rhob)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: Tb
        REAL(kind=8),INTENT(in)  :: rhob
       !Local Variables
        REAL(kind=8)    :: mu1
        REAL(kind=8)    :: Ts
        REAL(kind=8)    :: rhos
        REAL(kind=8)    :: temp1
        REAL(kind=8)    :: temp2
        INTEGER         :: i
        INTEGER         :: j

        Ts=1.0 /Tb - 1.0 
        rhos=rhob-1.0 
        temp1=0.0 
        DO i=1,6
            temp2=0.0 
            DO j=1,7
                temp2=temp2+mu1H(i,j)*(rhos**(j-1 ))
            END DO
            temp1=temp1+(Ts**(i-1 ))*temp2
        END DO
        mu1=exp(rhob*temp1)
        RETURN
    END FUNCTION mu1

    FUNCTION mu(T,rho)
        IMPLICIT NONE
       !Arguments
        REAL(kind=8),INTENT(in)  :: T
        REAL(kind=8),INTENT(in)  :: rho
       !Local Variables
        REAL(kind=8)    :: mu
        REAL(kind=8)    :: Tb
        REAL(kind=8)    :: rhob    

        Tb=T/647.096 
        rhob=rho/322.0 
        mu=mu0(Tb)*mu1(Tb,rhob)*1.0E-6 
    END FUNCTION mu

END MODULE SteamProp
