!subcooled density 
FUNCTION scdensity(p,T)
    USE SteamProp, ONLY: dGdpR1, psR1, TsR1, R
    IMPLICIT NONE
   !Arguments
    REAL(kind=8),INTENT(in)  :: p
    REAL(kind=8),INTENT(in)  :: T
   !Local Variables
    REAL(kind=8)    :: scdensity
    REAL(kind=8)    :: pMPa
    REAL(kind=8)    :: pi
    REAL(kind=8)    :: tau
    REAL(kind=8)    :: dGdp

    pMPa = p*1.0E-6 
    pi   = pMPa/psR1
    tau  = TsR1/T
    dGdp = dGdpR1(pi,tau)
    scdensity = 1000.0 *psR1/(R*T*dGdp)
    RETURN
END FUNCTION scdensity
!derivative of density wrt pressure in subcooled region
FUNCTION scddendp(p,T)
    USE SteamProp, ONLY: dGdpR1, d2Gdp2R1, psR1, TsR1, R
    IMPLICIT NONE
   !Arguments
    REAL(kind=8),INTENT(in)  :: p
    REAL(kind=8),INTENT(in)  :: T
   !Local Variables
    REAL(kind=8)    :: scddendp  
    REAL(kind=8)    :: pMPa
    REAL(kind=8)    :: pi
    REAL(kind=8)    :: tau
    REAL(kind=8)    :: dGdp
    REAL(kind=8)    :: d2Gdp2

    pMPa   = p*1.0E-6 
    pi     = pMPa/psR1
    tau    = TsR1/T
    dGdp   = dGdpR1(pi,tau)
    d2Gdp2 = d2Gdp2R1(pi,tau)
    scddendp = -0.001*d2Gdp2/(R*T*dGdp*dGdp)

END FUNCTION scddendp

!hfsat(p)
FUNCTION hfsatp(p)
    USE SteamProp, ONLY: Tsat, hR1
    IMPLICIT NONE
   !Arguments
    REAL(kind=8),INTENT(in)  :: p
   !Local Variables
    REAL(kind=8)    :: hfsatp
    REAL(kind=8)    :: T

    T = Tsat(p)
    hfsatp = hR1(p,T)
    RETURN
END FUNCTION hfsatp

!hgsat(p)
FUNCTION hgsatp(p)
    USE SteamProp, ONLY: Tsat, hR2
    IMPLICIT NONE
   !Arguments
    REAL(kind=8),INTENT(in)  :: p
   !Local Variables
    REAL(kind=8)    :: hgsatp
    REAL(kind=8)    :: T

    T = Tsat(p)
    hgsatp = hR2(p,T)
    RETURN
END FUNCTION hgsatp

!vfsat(p)
FUNCTION vfsatp(p)
    USE SteamProp, ONLY: Tsat, vR1
    IMPLICIT NONE
   !Arguments
    REAL(kind=8),INTENT(in)  :: p
   !Local Variables
    REAL(kind=8)    :: vfsatp
    REAL(kind=8)    :: T

    T = Tsat(p)
    vfsatp = vR1(p,T)
    RETURN
END FUNCTION vfsatp

!vgsat(p)
FUNCTION vgsatp(p)
    USE SteamProp, ONLY: Tsat, vR2
    IMPLICIT NONE
   !Arguments
    REAL(kind=8),INTENT(in)  :: p
   !Local Variables
    REAL(kind=8)    :: vgsatp
    REAL(kind=8)    :: T

    T = Tsat(p)
    vgsatp = vR2(p,T)
    RETURN
END FUNCTION vgsatp



!tsatp(p)
FUNCTION tsatp(p)
    USE SteamProp, ONLY: Tsat
    IMPLICIT NONE
   !Arguments
    REAL(kind=8),INTENT(in)  :: p
   !Local Variables
    REAL(kind=8)    :: tsatp

    tsatp = Tsat(p)
    RETURN
END FUNCTION tsatp