#!/usr/bin/env python

import steam

print ''
print 'Testing properties vs book reference (ASME Steam Tables) ...'

TC   = 200.0
pMPa = 2.0
vR   = 0.0011561  # value taken from ASME Internatinoal Steam Table for Industrial Use

T = TC + 273.15
p = pMPa * 1.0E6

denR = 1/vR
den = steam.scdensity(p,T)

print ''
print '{0:15s}|{1:15s}|{2:15s}'.format('Value','Reference','Code')
print '{0:15s}|{1:15s}|{2:15s}'.format('---------------','---------------','---------------')
print '{0:15s}|{1:15e}|{2:15e}'.format('density',denR,den)

ddendp = steam.scddendp(p,T)


print ''
print 'Testing ddendp via finite difference ...'
print ''
print '{0:15s}|{1:15s}|{2:15s}|{3:15s}'.format('dp','Code','FD','err')
print '{0:15s}|{1:15s}|{2:15s}|{3:15s}'.format('---------------','---------------','---------------','---------------')

dp = 0.1*p
denp = steam.scdensity(p+dp,T)
dden = denp - den
ddendpN = dden/dp
err = ddendpN - ddendp
print '{0:15e}|{1:15e}|{2:15e}|{3:15e}'.format(dp,ddendp,ddendpN,err)

dp = 0.01*p
denp = steam.scdensity(p+dp,T)
dden = denp - den
ddendpN = dden/dp
err = ddendpN - ddendp
print '{0:15e}|{1:15s}|{2:15e}|{3:15e}'.format(dp,'',ddendpN,err)

dp = 0.001*p
denp = steam.scdensity(p+dp,T)
dden = denp - den
ddendpN = dden/dp
err = ddendpN - ddendp
print '{0:15e}|{1:15s}|{2:15e}|{3:15e}'.format(dp,'',ddendpN,err)
print ''

p = 20.0
hf = steam.hfsatp(p)
hg = steam.hgsatp(p)
print hf
print hg